package com.khc.sqlSession;

import com.khc.pojo.Configuration;
import com.khc.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

/**
 * @param
 * @return
 */
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;
    private static final String VOID = "void";

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementid, Object... params) throws Exception {
        //将要去完成对simpleExecutor里的query方法的调用
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementid);
        List<Object> list = simpleExecutor.query(configuration, mappedStatement, params);
        return (List<E>) list;
    }

    @Override
    public <T> T selectOne(String statementid, Object... paramter) throws Exception {
        //调用查询所有的方法，如果只有一条记录说明查询成功，若有多条记录说明查询失败
        List<Object> objects = selectList(statementid, paramter);
        if(objects.size()==1){
            return (T) objects.get(0);
        }else{
            throw new RuntimeException("查询结果为空或者返回结果过多");
        }
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        //使用JDK动态代理来为Dao接口生成代理对象，并返回
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            //代理对象调用接口中任意方法，都会执行invoke方法
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //proxy：当前代理对象的应用       method：当前调用方法的引用        args：被调用方法传递过来的参数
                //底层都是去执行JDBC的代码，根据情况不同，来调用selectList或selectOne
                //准备参数：1.statementId：namespace.id=接口全限定名.方法名
                String methodName = method.getName();//方法名：findAll
                String className = method.getDeclaringClass().getName();//接口类的全限定名
                String statementId=className+"."+methodName;


                //准备参数2：params：args
                //获取被调用方法的返回值类型，如果是List集合则调用selectList方法，如果是单个对象则调用selectOne方法
                Type genericReturnType = method.getGenericReturnType();//获得调用方法的返回值类型
                //判断是否进行了 泛型类型参数化       就是看你List后面有没有跟泛型
                if(genericReturnType instanceof ParameterizedType){
                    List<Object> objects = selectList(statementId,args);
                    return objects;
                }
                if(genericReturnType instanceof Class && VOID.equals(genericReturnType.getTypeName())) {
                    updateUser(statementId, args);
                    return null;
                }
                return selectOne(statementId,args);
            }
        });
        return (T) proxyInstance;
    }

    @Override
    public void insertUser(String statementid, Object... paramter) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementid);
        simpleExecutor.insert(configuration,mappedStatement,paramter);
    }

    @Override
    public void updateUser(String statementid, Object... paramter) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementid);
        simpleExecutor.update(configuration,mappedStatement,paramter);
    }

    @Override
    public void deleteUser(String statementid, Object... paramter) throws Exception {
        simpleExecutor simpleExecutor = new simpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementid);
        simpleExecutor.delete(configuration,mappedStatement,paramter);
    }
}
