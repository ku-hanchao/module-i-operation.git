package com.khc.sqlSession;

import com.khc.pojo.Configuration;
import com.khc.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

/**
 * @param
 * @return
 */
public interface Executor {
    //三个参数：第一个是数据库配置信息，第二个是：封装的数据库语句，第三个是：可变参数
    //实现底层的查询方法
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement,Object...paramter) throws SQLException, Exception;

    //实现插入方法
    public void insert(Configuration configuration, MappedStatement mappedStatement,Object...paramter) throws Exception;

    //实现更新方法
    public void update(Configuration configuration, MappedStatement mappedStatement,Object...paramter) throws Exception;

    //实现删除方法
    public void delete(Configuration configuration, MappedStatement mappedStatement,Object...paramter) throws Exception;
}
