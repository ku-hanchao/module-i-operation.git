package com.khc.sqlSession;

import java.util.List;

/**
 * @param
 * @return
 */
public interface SqlSession {
    //查询所有
    public <E> List<E> selectList(String statementid,Object...paramter) throws Exception;//可变参数：Object...paramter，其中泛型不确定，就用E代替

    //查询单个
    public <T> T selectOne(String statementid,Object...paramter) throws Exception;

    //为Dao接口生成代理实现类
    public <T> T getMapper(Class<?> mapperClass);

    public void insertUser(String statementid,Object...paramter) throws Exception;

    public void updateUser(String statementid,Object...paramter) throws Exception;

    public void deleteUser(String statementid,Object...paramter) throws Exception;
}
