package com.khc.sqlSession;

/**
 * @param
 * @return
 */
public interface SqlsessionFactory {

    public SqlSession openSession();
}
