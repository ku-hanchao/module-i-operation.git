package com.khc.sqlSession;

import com.khc.config.BoundSql;
import com.khc.pojo.Configuration;
import com.khc.pojo.MappedStatement;
import com.khc.utils.GenericTokenParser;
import com.khc.utils.ParameterMapping;
import com.khc.utils.ParameterMappingTokenHandler;
import com.khc.utils.TokenHandler;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.khc.utils.ClassType.getClassType;

/**
 * @param
 * @return
 */
public class simpleExecutor implements Executor {
    @Override                                                                                //user
    public <E> List<E> query(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {

        //这里进行参数的转换，获得sql的预处理对象
        ParamterHander paramterHander = new ParamterHander(configuration,mappedStatement,params);
        PreparedStatement preparedStatement = paramterHander.getPreparedStatement();

        //执行sql
        ResultSet resultSet = preparedStatement.executeQuery();
        resultHander resultHander = new resultHander(mappedStatement, resultSet);
        List<Object> objects = resultHander.getresultHander();
        return (List<E>) objects;

    }

    @Override
    public void insert(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //这里进行参数的转换，获得sql的预处理对象
        ParamterHander paramterHander = new ParamterHander(configuration,mappedStatement,params);
        PreparedStatement preparedStatement = paramterHander.getPreparedStatement();
        preparedStatement.executeUpdate();
    }

    @Override
    public void update(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
        //这里进行参数的转换，获得sql的预处理对象
        ParamterHander paramterHander = new ParamterHander(configuration,mappedStatement,params);
        PreparedStatement preparedStatement = paramterHander.getPreparedStatement();
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception {
//这里进行参数的转换，获得sql的预处理对象
        ParamterHander paramterHander = new ParamterHander(configuration,mappedStatement,params);
        PreparedStatement preparedStatement = paramterHander.getPreparedStatement();
        preparedStatement.executeUpdate();
    }




}
