package com.khc.sqlSession;

import com.khc.config.XMLConfigBuilder;
import com.khc.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * @param
 * @return
 */
public class SqlSessionFactoryBuilder {

    public SqlsessionFactory build(InputStream in) throws DocumentException, PropertyVetoException {
         //第一：使用dom4j解析配置文件，将解析的内容封装到Configuration中
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(in);
        
        //第二：创建SqlsessionFactory对象
        DefaultSqlSessionFactory defaultSqlSessionFactory = new DefaultSqlSessionFactory(configuration);


        return defaultSqlSessionFactory;
    }
}
