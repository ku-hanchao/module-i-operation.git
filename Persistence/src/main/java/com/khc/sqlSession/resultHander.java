package com.khc.sqlSession;

import com.khc.pojo.MappedStatement;

import java.beans.PropertyDescriptor;
import java.io.ObjectStreamClass;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

import static com.khc.utils.ClassType.getClassType;

/**
 * @param
 * @return
 */
public class resultHander {
    private MappedStatement mappedStatement;
    private ResultSet resultSet;

    public resultHander(MappedStatement mappedStatement, ResultSet resultSet) {
        this.mappedStatement = mappedStatement;
        this.resultSet = resultSet;
    }

    public <E> List<E> getresultHander() throws Exception {
        //拿到返回结果的全路径,并获得对应的class对象
        String resultType = mappedStatement.getResultType();//拿到返回结果集的全路径

        Class<?> resultTypeClass = getClassType(resultType);//转换为class对象

        ArrayList<Object> objects = new ArrayList<>();
        //6.封装返回结果集
        while(resultSet.next()){
            Object o = resultTypeClass.newInstance();//获得class对象的具体实现
            //元数据
            ResultSetMetaData metaData = resultSet.getMetaData();
            for (int i = 1; i <=metaData.getColumnCount() ; i++) {//根据列数进行循环，例如id，name就是两列
                //字段名称
                String columnName = metaData.getColumnName(i);
                //根据字段名称获得值
                Object value = resultSet.getObject(columnName);
                //使用反射或者内省，根据数据库表和实体的对应关系，完成封装
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(columnName,resultTypeClass);//class对象对columnName生成读写方法
                Method writeMethod = propertyDescriptor.getWriteMethod();
                writeMethod.invoke(o,value);//将具体的value值封装在了o对象中
            }
            objects.add(o);
        }
        return (List<E>) objects;
    }
}
