package com.khc.sqlSession;

import com.khc.config.BoundSql;
import com.khc.pojo.Configuration;
import com.khc.pojo.MappedStatement;
import com.khc.utils.GenericTokenParser;
import com.khc.utils.ParameterMapping;
import com.khc.utils.ParameterMappingTokenHandler;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.khc.utils.ClassType.getClassType;

/**
 * @param
 * @return
 */
public class ParamterHander {
    private Configuration configuration;
    private MappedStatement mappedStatement;
    private Object[] params;

    public ParamterHander(Configuration configuration, MappedStatement mappedStatement, Object[] params) {
        this.configuration = configuration;
        this.mappedStatement = mappedStatement;
        this.params = params;

    }

    //该方法为了得到参数已经转换好的sql预处理对象
    public PreparedStatement getPreparedStatement() throws Exception {
        //1.注册驱动，获取连接
        Connection connection = configuration.getDataSource().getConnection();

        //2.获取sql语句：select * from user where id = #{id} and username = #{username}
        //其中要对#{}中的参数进行解析
        String sql = mappedStatement.getSql();
        BoundSql boundSql = getBoundSql(sql);

        //3.获得预处理对象：preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());

        //4.设置参数
        //获取到参数的全路径
        String paramterType = mappedStatement.getParamterType();
        Class<?> paramterClass = getClassType(paramterType);//获取传入参数类型

        List<ParameterMapping> parameterMappingList = boundSql.getParameterMappingList();
        for (int i = 0; i < parameterMappingList.size(); i++) {
            ParameterMapping parameterMapping = parameterMappingList.get(i);
            String content = parameterMapping.getContent();//获取到#{}中的值

            //反射
            Field declaredField = paramterClass.getDeclaredField(content);//根据class对象中的属性名获取到具体属性
            declaredField.setAccessible(true);//防止该类的属性私有的，可以暴力访问
            Object o = declaredField.get(params[0]);//这里拿到的就是具体属性的值
            preparedStatement.setObject(i + 1, o);
        }

        return preparedStatement;
    }



    /*
     * 完成对#{}的解析工作：1、将#{}用？代替  2.解析出#{}中值得存储
     *
     * */
    private BoundSql getBoundSql (String sql){
        //标记处理类：配合标记解析器完成对占位符的解析处理工作
        ParameterMappingTokenHandler parameterMappingTokenHandler = new ParameterMappingTokenHandler();
//        标记解析器
        GenericTokenParser genericTokenParser = new GenericTokenParser("#{", "}", parameterMappingTokenHandler);
        //解析出来的sql
        String parseSql = genericTokenParser.parse(sql);
        //解析出来的#{}中的参数名称
        List<ParameterMapping> parameterMappings = parameterMappingTokenHandler.getParameterMappings();
        BoundSql boundSql = new BoundSql(parseSql, parameterMappings);

        return boundSql;
    }


}







