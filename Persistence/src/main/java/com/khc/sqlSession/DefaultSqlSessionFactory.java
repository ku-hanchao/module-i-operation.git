package com.khc.sqlSession;

import com.khc.pojo.Configuration;

/**
 * @param
 * @return
 */
public class DefaultSqlSessionFactory implements SqlsessionFactory {
    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
