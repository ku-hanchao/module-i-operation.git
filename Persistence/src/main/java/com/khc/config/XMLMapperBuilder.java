package com.khc.config;

import com.khc.pojo.Configuration;
import com.khc.pojo.MappedStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * @param
 * @return
 */
public class XMLMapperBuilder {
    private Configuration configuration;
    public XMLMapperBuilder(Configuration configuration) {
        this.configuration=configuration;
    }

    public void parse(InputStream inputStream) throws DocumentException {
        Document document = new SAXReader().read(inputStream);//获得文档对象
        Element rootElement = document.getRootElement();//获得文档中的根对象
        String namespace = rootElement.attributeValue("namespace");
        List<Element> selectlist = rootElement.selectNodes("//select");
        elementToMapperstatement(selectlist,namespace);
        List<Element> insertlist = rootElement.selectNodes("//insert");
        elementToMapperstatement(insertlist,namespace);
        List<Element> updatelist = rootElement.selectNodes("//update");
        elementToMapperstatement(updatelist,namespace);
        List<Element> deletelist = rootElement.selectNodes("//delete");
        elementToMapperstatement(deletelist,namespace);
    }

    //该方法将statementid封装到configuration中去
    public void elementToMapperstatement(List<Element> list,String namespace){
        for (Element element : list) {//封装成mappedstatement对象
            String id = element.attributeValue("id");
            String resultType = element.attributeValue("resultType");
            String paramterType = element.attributeValue("paramterType");
            String sqlText = element.getTextTrim();//得到sql语句
            MappedStatement mappedStatement = new MappedStatement();
            mappedStatement.setId(id);
            mappedStatement.setResultType(resultType);
            mappedStatement.setSql(sqlText);
            mappedStatement.setParamterType(paramterType);
            String key = namespace + "." + id;
            configuration.getMappedStatementMap().put(key, mappedStatement);
        }
    }
}
