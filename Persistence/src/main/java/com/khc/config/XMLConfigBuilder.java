package com.khc.config;

import com.khc.io.Resources;
import com.khc.pojo.Configuration;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.annotation.Resource;
import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * @param
 * @return
 */
public class XMLConfigBuilder {
    private Configuration configuration;

    //利用XMLconfigbuilder的无参对象对configuration进行赋值
    public XMLConfigBuilder() {
        this.configuration = new Configuration();
    }

    /*
*
* 该方法就是使用dom4j对配置文件解析，封装configuration
*
* */
    public Configuration parseConfig(InputStream inputStream) throws DocumentException, PropertyVetoException {
        Document document = new SAXReader().read(inputStream);
        //拿到根标签<configuration>
        Element rootElement = document.getRootElement();
        List<Element> list = rootElement.selectNodes("//property");
        Properties properties = new Properties();
        for (Element element : list) {
            //通过循环遍历获得configuration中的数据，具体是键和值的对应
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.setProperty(name,value);
        }
        //创建连接池对象
        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(properties.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(properties.getProperty("username"));
        comboPooledDataSource.setPassword(properties.getProperty("password"));
        configuration.setDataSource(comboPooledDataSource);

        //mapper.xml解析:拿到路径，然后用dom4j进行解析
        List<Element> mapperlist = rootElement.selectNodes("//mapper");//获得mapper属性
        for (Element element : mapperlist) {
            String mapperPath = element.attributeValue("resource");//获取resource中UserMapper的路径值
            InputStream resourceAsSteam = Resources.getResourceAsSteam(mapperPath);//将路径值转换成字节流
            //创建XMLMapperBuilder类封装dom4j，解析UserMapper文件
            XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(configuration);
            xmlMapperBuilder.parse(resourceAsSteam);
        }

        return configuration;
    }
}
