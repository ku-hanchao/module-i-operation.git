package com.khc.utils;

/**
 * @param
 * @return
 */
public class ClassType {
    //方法的作用就是根据某一个类的全路径获取到其Class对象
    public static Class<?> getClassType(String paramterType) throws ClassNotFoundException {
        if (paramterType != null) {
            Class<?> aClass = Class.forName(paramterType);
            return aClass;
        }
        return null;
    }
}
