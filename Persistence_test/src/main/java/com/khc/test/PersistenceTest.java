package com.khc.test;

import com.khc.dao.IUserDao;
import com.khc.io.Resources;
import com.khc.pojo.User;
import com.khc.sqlSession.SqlSession;
import com.khc.sqlSession.SqlSessionFactoryBuilder;
import com.khc.sqlSession.SqlsessionFactory;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;

/**
 * @param
 * @return
 */
public class PersistenceTest {
    @Test
    public void test() throws Exception {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlsessionFactory sqlsessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlsessionFactory.openSession();


        User user = new User();
        user.setId(1);
        user.setUsername("张三");
//        调用
//        User user2 = sqlSession.selectOne("user.selectOne", user);
//        System.out.println(user2);
//        List<User> users = sqlSession.selectList("user.selectList");
//        for (User user1 : users) {
//            System.out.println(user1);
//        }
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);


//        User user2 = userDao.findByCondition(user);
//        System.out.println(user2);
        List<User> user2 = userDao.findAll();
        for (User user1 : user2) {
            System.out.println(user1);
        }
    }

    //测试insert方法
    @Test
    public void test1() throws PropertyVetoException, DocumentException {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlsessionFactory sqlsessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlsessionFactory.openSession();
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);


        User user = new User();
        user.setId(4);
        user.setUsername("王笛");
        userDao.addUser(user);
    }

    //测试update方法
    @Test
    public void test2() throws PropertyVetoException, DocumentException {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlsessionFactory sqlsessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlsessionFactory.openSession();
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        User user = new User();
        user.setId(1);
        user.setUsername("王五");
        userDao.updateUser(user);
    }

    //测试delete方法
    @Test
    public void test3() throws PropertyVetoException, DocumentException {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlsessionFactory sqlsessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlsessionFactory.openSession();
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        User user = new User();
        user.setId(1);
        userDao.deleteUser(user);
    }
}
